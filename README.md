# 動作確認済
Python 3.11.2

# 環境構築
```
git clone https://gitlab.com/suzukidaisuke/fushinsha_map.git
cd fushinsha_map
git submodule update --init --recursive
poetry install
```

# 実行
fushinsha_map.ipynb を上からポチポチと実行。

もしくは `poetry run ipython fushinsha_map.ipynb` と実行。

master push すると html が公開される。

# その他

**フォーマット**

```
poetry run ruff format .
```

**lint チェック**

```
poetry run ruff check .
```
